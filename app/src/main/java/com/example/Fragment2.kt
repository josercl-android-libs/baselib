package com.example

import android.view.View
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import com.example.databinding.Fragment2Binding
import com.gitlab.josercl.android.baselib.Modelable
import com.gitlab.josercl.android.baselib.ViewBindable
import com.gitlab.josercl.android.baselib.fragment.BaseFragment

class Fragment2: BaseFragment(), ViewBindable<Fragment2Binding>, Modelable<Fragment2ViewModel> {
    private lateinit var binding: Fragment2Binding
    private lateinit var viewModel: Fragment2ViewModel

    override fun inflateViewBinding(): Fragment2Binding =
        Fragment2Binding.inflate(layoutInflater)

    override fun setViewBinding(binding: Fragment2Binding) {
        this.binding = binding
    }

    override fun initViewModel(): Fragment2ViewModel {
        return ViewModelProvider(this)[Fragment2ViewModel::class.java]
    }

    override fun setViewModel(model: Fragment2ViewModel) {
        this.viewModel = model
    }

    override fun setupObservers() {
        viewModel.text.observe(this) {
            binding.text1.text = it
        }
    }

    override fun setupUI(view: View) {
        binding.button1.setOnClickListener {
            navController.popBackStack()
        }
    }

    /*override fun inflateDataBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): Fragment2Binding = Fragment2Binding.inflate(inflater, container ,false)

    override fun bindVariables() {
        binding.model = viewModel
    }

    override fun initViewModel(): Fragment2ViewModel {
        return ViewModelProvider(this)[Fragment2ViewModel::class.java]
    }

    override fun setupObservers() {}

    override fun injectDependencies(context: Context) {}

    override fun setupUI(view: View) {
        binding.button1.setOnClickListener {
            navController.popBackStack()
        }
    }*/
}