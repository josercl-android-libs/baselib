package com.example

import com.example.databinding.ActivityMainBinding
import com.gitlab.josercl.android.baselib.ViewBindable
import com.gitlab.josercl.android.baselib.activity.BaseActivity

class MainActivity : BaseActivity(), ViewBindable<ActivityMainBinding> {
    private lateinit var binding: ActivityMainBinding

    override fun inflateViewBinding(): ActivityMainBinding =
        ActivityMainBinding.inflate(layoutInflater)

    override fun setViewBinding(binding: ActivityMainBinding) {
        this.binding = binding
    }

    override fun setupUI() {

    }
    /*override fun inflateViewBinding(): ActivityMainBinding =
        ActivityMainBinding.inflate(layoutInflater)

    override fun setupUI() {}*/
}
