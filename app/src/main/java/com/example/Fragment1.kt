package com.example

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.databinding.Fragment1Binding
import com.gitlab.josercl.android.baselib.DataBindableFragment
import com.gitlab.josercl.android.baselib.fragment.BaseFragment

class Fragment1: BaseFragment(), DataBindableFragment<Fragment1Binding> {
    private lateinit var binding: Fragment1Binding

    override fun inflateDataBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): Fragment1Binding = Fragment1Binding.inflate(inflater, container, false)

    override fun setDataBinding(binding: Fragment1Binding) {
        this.binding = binding
    }

    override fun getDataBinding(): Fragment1Binding = binding

    override fun bindVariables() {
        binding.text = "This is the initial text"
    }

    override fun setupUI(view: View) {
        binding.button1.setOnClickListener {
            navController.navigate(Fragment1Directions.from1To2())
        }

        binding.button2.setOnClickListener {
            runWithPermissions(arrayOf(android.Manifest.permission.CAMERA), 100) {
                binding.text = "Text Changed with code"
            }
        }
    }
}