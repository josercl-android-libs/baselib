package com.example

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class Fragment2ViewModel : ViewModel() {
    val text = MutableLiveData("This is fragment 2")
}