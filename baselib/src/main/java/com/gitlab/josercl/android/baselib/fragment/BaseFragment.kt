/*
 * Copyright (c) 2020 José Rafael Carrero León <josercl@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package com.gitlab.josercl.android.baselib.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.PermissionChecker
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.gitlab.josercl.android.baselib.DataBindableFragment
import com.gitlab.josercl.android.baselib.Modelable
import com.gitlab.josercl.android.baselib.PermissionsResultHandler
import com.gitlab.josercl.android.baselib.ViewBindable

abstract class BaseFragment : Fragment() {
    protected val navController: NavController by lazy { findNavController() }
    private val permissionResultsHandlerList: ArrayList<PermissionsResultHandler> = arrayListOf()

    abstract fun setupUI(view: View)
    protected open fun injectDependencies(context: Context) {}

    override fun onAttach(context: Context) {
        super.onAttach(context)
        injectDependencies(context)

        if (this is Modelable<*>) {
            val viewModel = initViewModel()
            setModel(viewModel)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = when (this) {
        is DataBindableFragment<*> -> {
            val binding = inflateDataBinding(inflater, container)
            setBinding(binding)
            binding.root
        }
        is ViewBindable<*> -> {
            val viewBinding = inflateViewBinding()
            setBinding(viewBinding)
            viewBinding.root
        }
        else -> {
            super.onCreateView(inflater, container, savedInstanceState)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupUI(view)

        if (this is DataBindableFragment<*>) {
            bindVariables()
            getDataBinding().lifecycleOwner = this
        }

        if (this is Modelable<*>) {
            setupObservers()
        }
    }

    final override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        val hasPermissions = grantResults.isNotEmpty() &&
                grantResults.all { it == PermissionChecker.PERMISSION_GRANTED }
        if (hasPermissions) {
            permissionResultsHandlerList.firstOrNull { it.canHandle(requestCode) }?.execute()
        }
    }

    protected fun runWithPermissions(
        permissions: Array<String>,
        requestCode: Int,
        function: () -> Unit
    ) {
        if (permissions.isEmpty()) return

        if (!canHandle(requestCode)) {
            registerPermissionResultHandler(requestCode, function)
        }

        val hasAllPermissions = permissions.all {
            PermissionChecker.checkSelfPermission(
                requireContext(),
                it
            ) == PermissionChecker.PERMISSION_GRANTED
        }
        if (hasAllPermissions) {
            function()
        } else {
            requestPermissions(permissions, requestCode)
        }
    }

    private fun canHandle(requestCode: Int): Boolean = permissionResultsHandlerList.isNotEmpty() &&
            permissionResultsHandlerList.any { it.canHandle(requestCode) }

    private fun registerPermissionResultHandler(requestCode: Int, function: () -> Unit) {
        permissionResultsHandlerList.add(
            object : PermissionsResultHandler {
                override fun canHandle(code: Int): Boolean = requestCode == code

                override fun execute() = function()
            }
        )
    }
}