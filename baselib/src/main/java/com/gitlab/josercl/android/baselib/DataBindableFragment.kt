/*
 * Copyright (c) 2020 José Rafael Carrero León <josercl@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package com.gitlab.josercl.android.baselib

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.ViewDataBinding

interface DataBindableFragment<DB : ViewDataBinding> {
    fun inflateDataBinding(inflater: LayoutInflater, container: ViewGroup?): DB
    fun setDataBinding(binding: DB)
    fun getDataBinding(): DB
    fun bindVariables()

    @Suppress("UNCHECKED_CAST")
    fun setBinding(binding: ViewDataBinding) {
        val tempBinding = binding as? DB
            ?: throw IllegalArgumentException("Invalid type ${binding.javaClass.name} passed to setBinding")
        setDataBinding(tempBinding)
    }
}