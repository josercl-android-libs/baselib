/*
 * Copyright (c) 2020 José Rafael Carrero León <josercl@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */


package com.gitlab.josercl.android.baselib.activity

import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.PermissionChecker
import com.gitlab.josercl.android.baselib.DataBindable
import com.gitlab.josercl.android.baselib.Modelable
import com.gitlab.josercl.android.baselib.PermissionsResultHandler
import com.gitlab.josercl.android.baselib.ViewBindable

abstract class BaseActivity : AppCompatActivity() {

    private val permissionResultsHandlerList: ArrayList<PermissionsResultHandler> = arrayListOf()

    abstract fun setupUI()

    protected open fun injectDependencies(context: Context) {}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        injectDependencies(this)

        if (this is ViewBindable<*> && this is DataBindable<*>) {
            throw RuntimeException("An activity can't use ViewBinding and DataBinding at the same time")
        }

        if (this is ViewBindable<*>) {
            val viewBinding = inflateViewBinding()
            setBinding(viewBinding)
            setContentView(viewBinding.root)
        }

        if (this is DataBindable<*>) {
            val dataBinding = inflateDataBinding()
            setBinding(dataBinding)
            setContentView(dataBinding.root)

            dataBinding.lifecycleOwner = this
        }

        if (this is Modelable<*>) {
            setModel(initViewModel())
        }

        setupUI()

        if (this is DataBindable<*>) {
            bindVariables()
        }

        if (this is Modelable<*>) {
            setupObservers()
        }
    }

    final override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        val hasPermissions = grantResults.isNotEmpty() &&
                grantResults.all { it == PermissionChecker.PERMISSION_GRANTED }
        if (hasPermissions) {
            permissionResultsHandlerList.firstOrNull { it.canHandle(requestCode) }?.execute()
        }
    }

    protected fun runWithPermissions(
        permissions: Array<String>,
        requestCode: Int,
        function: () -> Unit
    ) {
        if (permissions.isEmpty()) return

        if (!canHandle(requestCode)) {
            registerPermissionResultHandler(requestCode, function)
        }

        val hasAllPermissions = permissions.all {
            PermissionChecker.checkSelfPermission(
                this,
                it
            ) == PermissionChecker.PERMISSION_GRANTED
        }
        if (hasAllPermissions) {
            function()
        } else {
            ActivityCompat.requestPermissions(this, permissions, requestCode)
        }
    }

    private fun canHandle(requestCode: Int): Boolean = permissionResultsHandlerList.isNotEmpty() &&
            permissionResultsHandlerList.any { it.canHandle(requestCode) }

    private fun registerPermissionResultHandler(requestCode: Int, function: () -> Unit) {
        permissionResultsHandlerList.add(
            object :
                PermissionsResultHandler {
                override fun canHandle(code: Int): Boolean = requestCode == code

                override fun execute() = function()
            }
        )
    }
}