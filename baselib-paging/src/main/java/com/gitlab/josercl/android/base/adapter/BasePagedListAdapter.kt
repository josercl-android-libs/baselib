/*
 * Copyright (c) 2020 José Rafael Carrero León <josercl@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package com.gitlab.josercl.android.base.adapter

import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import com.gitlab.josercl.android.baselib.adapter.BaseViewHolder

abstract class BasePagedListAdapter<Item, VH : BaseViewHolder<Item>>(
    areItemsTheSame: (oldItem: Item, newItem: Item) -> Boolean,
    areContentsTheSame: (oldItem: Item, newItem: Item) -> Boolean,
    changePayload: ((oldItem: Item, newItem: Item) -> Any?)? = null
) : PagedListAdapter<Item, VH>(object : DiffUtil.ItemCallback<Item>() {
    override fun areItemsTheSame(oldItem: Item, newItem: Item) =
        areItemsTheSame(oldItem, newItem)

    override fun areContentsTheSame(oldItem: Item, newItem: Item) =
        areContentsTheSame(oldItem, newItem)

    override fun getChangePayload(oldItem: Item, newItem: Item): Any? {
        return changePayload?.invoke(oldItem, newItem) ?: super.getChangePayload(oldItem, newItem)
    }
}) {
    override fun onBindViewHolder(holder: VH, position: Int) = holder.bindTo(getItem(position))
}